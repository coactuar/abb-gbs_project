-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2022 at 05:37 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abb_gbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_batch` varchar(50) NOT NULL,
  `user_loc` varchar(500) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_batch`, `user_loc`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'viraj', 'batch1', 'PUNE', 'ARE YOU INFECTED??', '2020-03-20 09:36:39', 'abb-gbs', 0, 0),
(2, 'Pawan ', 'batch1', 'Coact Pune ', 'Ppt n audio visible', '2020-03-20 09:37:28', 'abb-gbs', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_location` varchar(500) NOT NULL,
  `user_batch` varchar(50) NOT NULL,
  `joining_date` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_location`, `user_batch`, `joining_date`, `eventname`) VALUES
(1, 'Sujatha', 'Bangalore ', 'batch1', '2020-03-19 16:41:19', 'abb-gbs'),
(2, 'Viraj', 'Pune', 'batch1', '2020-03-19 17:26:39', 'abb-gbs'),
(3, 'viraj', 'PUNE', 'batch1', '2020-03-19 17:29:51', 'abb-gbs'),
(4, 'Jenitha', 'Bangalore', 'batch1', '2020-03-19 18:48:37', 'abb-gbs'),
(5, 'Jenitha', 'Bangalore', 'batch1', '2020-03-19 18:53:36', 'abb-gbs'),
(6, 'Akshat', 'Bangalore', 'batch1', '2020-03-20 09:29:37', 'abb-gbs'),
(7, 'viraj', 'PUNE', 'batch1', '2020-03-20 09:35:10', 'abb-gbs'),
(8, 'Pawan ', 'Coact Pune ', 'batch1', '2020-03-20 09:37:02', 'abb-gbs'),
(9, 'Sujatha', 'Bangalore', 'batch1', '2020-03-20 09:38:02', 'abb-gbs'),
(10, 'Akshat', 'Bangalore', 'batch1', '2020-03-20 09:39:59', 'abb-gbs'),
(11, 'Pooja', 'Mumbai', 'batch1', '2020-03-20 09:42:10', 'abb-gbs'),
(12, 'Pooja', 'Mumbai', 'batch1', '2020-03-20 09:43:02', 'abb-gbs'),
(13, 'IE ', 'COACT MUMBAI', 'batch1', '2020-03-20 09:43:10', 'abb-gbs'),
(14, 'chrome ', 'coact bangalore ', 'batch1', '2020-03-20 09:44:46', 'abb-gbs'),
(15, 'Harish V', 'Bangalore', 'batch1', '2020-03-20 09:59:20', 'abb-gbs'),
(16, 'Akshat', 'bangalore', 'batch1', '2020-03-20 10:10:48', 'abb-gbs'),
(17, 'viraj', 'pune', 'batch1', '2020-03-20 10:30:43', 'abb-gbs'),
(18, 'COACT hYD ', 'Hyderabad ', 'batch1', '2020-03-20 10:31:09', 'abb-gbs'),
(19, 'Chetan', 'Pune', 'batch1', '2020-03-20 11:19:52', 'abb-gbs'),
(20, 'akshat', 'Bng', 'batch1', '2021-10-12 11:27:07', 'abb-gbs'),
(21, 'akshat', 'Bng', 'batch1', '2021-10-12 11:27:07', 'abb-gbs'),
(22, 'Likitha', 'Bangalorw', 'batch1', '2021-12-27 16:28:18', 'abb-gbs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
