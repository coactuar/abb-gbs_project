<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABB :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row content">
        <div class="col-8 col-md-6 col-lg-4 offset-md-3 offset-lg-4 offset-2">
            <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <select name="batch" id="batch" class="form-control">
                        <option value="0">Select Session</option>
                        <option value="batch1">Batch 1: 11am to 1pm</option>
                        <option value="batch2">Batch 2: 2.30pm to 4.30pm</option>
                    </select>
                  </div>
                  
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <!--<div class="input-group">
                    <input type="text" class="form-control" placeholder="Employee ID" aria-label="Employee ID" aria-describedby="basic-addon1" name="empid" id="empid" required>
                  </div>-->
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Location" aria-label="Location" aria-describedby="basic-addon1" name="location" id="location" required>
                  </div>
                  
                  <div class="input-group">
                    <button id="login" class="btn btn-block btn-sm login-button" type="submit">Login</button>
                  </div>
                
            </form>        
        </div>
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
if($('#batch').val() == '0')
      {
          $('#login-message').text('Select batch');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>